package com.example.financialcontrol.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.financialcontrol.Models.Item;
import com.example.financialcontrol.R;

import java.text.SimpleDateFormat;
import java.util.List;

public class ItemAdapter extends BaseAdapter {
    List<Item> listItem;
    Context mContext;
    TextView txtIdentification;
    TextView txtType;
    TextView txtPrice;
    TextView txtCreationDate;

    public ItemAdapter(List<Item> listItem, Context context) {
        this.listItem = listItem;
        mContext = context;
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Item getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listItem.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View retorno = LayoutInflater.from(this.mContext).inflate(R.layout.item_adapter,parent,false);
        Item item = getItem(position);

        txtIdentification = retorno.findViewById(R.id.txtIdentification);
        txtType = retorno.findViewById(R.id.txtType);
        txtPrice = retorno.findViewById(R.id.txtPrice);
        txtCreationDate = retorno.findViewById(R.id.txtCreationDate);

        txtIdentification.setText(item.getIdentification());
        txtType.setText(item.getType().getType());
        txtPrice.setText(Double.toString(item.getPrice()));
        txtCreationDate.setText(new SimpleDateFormat().format(item.getCreationDate()));

        return retorno;
    }
}
