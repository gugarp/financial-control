package com.example.financialcontrol.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.financialcontrol.Adapters.ItemAdapter;
import com.example.financialcontrol.Business.CreditService;
import com.example.financialcontrol.Models.Item;
import com.example.financialcontrol.R;

import java.util.Date;
import java.util.List;

public class CreditFragment extends ListFragment {
    private List<Item> listItemCredit;
    private CreditService creditService;

    public CreditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        creditService = new CreditService();
        Date min = new Date(); min.setTime(new Date().getTime()-86400);
        Date max = new Date();
        listItemCredit = creditService.getItensByTypeAndPeriod(0,max,min);

        ItemAdapter itemAdapter = new ItemAdapter(listItemCredit,getContext());
        setListAdapter(itemAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_credit, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
