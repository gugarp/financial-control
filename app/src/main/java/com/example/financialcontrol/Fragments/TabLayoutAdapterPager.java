package com.example.financialcontrol.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.financialcontrol.R;

public class TabLayoutAdapterPager extends FragmentPagerAdapter {
    // TODO: Rename and change types of parameters
    private Context mContext;

    public TabLayoutAdapterPager(Context context, FragmentManager fm) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
            case 1:
            case 2:
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return mContext.getString(R.string.credt);
            case 1:
                return mContext.getString(R.string.debt);
            case 2:
                return mContext.getString(R.string.total);
            default:
                return null;
        }
    }
}
