package com.example.financialcontrol.Activitys;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.financialcontrol.Business.CreditService;
import com.example.financialcontrol.Enums.ItemTypes;
import com.example.financialcontrol.Models.Item;
import com.example.financialcontrol.Models.ItemType;
import com.example.financialcontrol.R;

import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {
    public Toolbar toolbar;
    private TabLayout main_tabLayout;
    private ViewPager view_Pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm.init(this);


        try {
            RegisterItemTypes();

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            view_Pager = findViewById(R.id.view_Pager);
            main_tabLayout = findViewById(R.id.main_tabLayout);
            main_tabLayout.addTab(main_tabLayout.newTab().setText(getResources().getString(R.string.credt)));
            main_tabLayout.addTab(main_tabLayout.newTab().setText(getResources().getString(R.string.debt)));
            main_tabLayout.addTab(main_tabLayout.newTab().setText(getResources().getString(R.string.total)));

            main_tabLayout.setupWithViewPager(view_Pager);

            setSupportActionBar(toolbar);
        }catch (Exception e){
            Toast.makeText(this,"Ocorreu um erro: "+e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }

    private void RegisterItemTypes() {
        CreditService creditService = new CreditService();
        List<ItemType> itemTypes = creditService.getItemTypes();
        if(itemTypes.size() == 0){
            for (int aux = 0; aux<3; aux++){
                ItemType itemType = new ItemType();
                switch (aux){
                    case 0: itemType.setType(getResources().getString(R.string.credt));
                    case 1: itemType.setType(getResources().getString(R.string.debt));
                    case 2: itemType.setType(getResources().getString(R.string.total));
                }

                creditService.RegisterItemType(itemType);
            }
        }
    }
}
