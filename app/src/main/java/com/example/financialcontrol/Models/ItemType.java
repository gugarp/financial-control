package com.example.financialcontrol.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ItemType extends RealmObject {
    @PrimaryKey
    private int Id;
    private String Type;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
