package com.example.financialcontrol.Business;

import com.example.financialcontrol.Models.Item;
import com.example.financialcontrol.Models.ItemType;

import java.lang.reflect.Type;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;

public class CreditService {
    private Realm realm;

    public CreditService() {
        this.realm = Realm.getDefaultInstance();
    }

    public RealmResults<ItemType> getItemTypes(){
        return realm.where(ItemType.class).findAll();
    }

    public RealmResults<Item> getItensByTypeId(int typeId){
        return realm.where(Item.class)
                .equalTo("Type",typeId)
                .findAll();
    }

    public RealmResults<Item> getItens(){
        return realm.where(Item.class).findAll();
    }

    public RealmResults<Item> getItensByPeriod(Date dateMax,Date datemin){
        return realm.where(Item.class)
                .between("CreationDate",dateMax,datemin)
                .findAll();
    }

    public RealmResults<Item> getItensByTypeAndPeriod(int typeId,Date dateMax,Date datemin){
        return realm.where(Item.class)
                .equalTo("Type",typeId).and()
                .between("CreationDate",dateMax,datemin)
                .findAll();
    }

    public void RegisterItemType(ItemType itemType) {
        realm.beginTransaction();
        realm.insertOrUpdate(itemType);
        realm.commitTransaction();
    }
}
